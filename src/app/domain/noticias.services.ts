import { Injectable } from "@angular/core";
import {getJSON, request} from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasServices{
    api:string = "https://4b9633b6345c.ngrok.io";
     private noticias : Array<string>=[];

     constructor(){
         this.getDb((db)=>{
            console.dir(db);
            db.each('select * from logs',
            (err,fila)=> {},
            (err,totales)=> {}
            )
         },()=>{
             console.error("error on getDB");
         });
     }
     
     agregar(s:string){
         return request({
             url: this.api + "/favs",
             method: "POST", 
             headers: {"Content-Type":"application/json"},
             content : JSON.stringify({
                 nuevo:s
             })
        });
     }

     favs(){
         return getJSON(this.api+"/favs");
     }

     buscar(s:string){
         this.getDb(db=>{
            db.execSQL("insert into logs (texto) values (?)",[s],
            (err,id)=> console.log("nuevo id: ", id));        
         },()=>{
             console.log("error on getDB");
         });
         return getJSON(this.api + "/get?q="+s);
     }


     getDb(fnOK,fnError){
         return new sqlite("mi_db_logs",(err,db)=>{
             if(err){
                 console.error("Error al abrir db!", err);
             }else{
                 console.log("Esta la db abierta: ", db.isOpen()? "si":"no");
                 db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                 .then(id=>{
                     console.log("CREATE TABLE OK");
                     fnOK(db);
                 },error=>{
                     console.log("CREATE TABLE ERROR",error);
                     fnError(error);
                 }
                 );
                }
         });
     }
}