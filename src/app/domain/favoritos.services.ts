import { Injectable } from '@angular/core';
import * as Toast from "nativescript-toasts";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class FavoritosService {

    

    getDb(fnOK,fnError){
        return new sqlite("mi_db_logs",(err,db)=>{
            if(err){
                console.error("Error al abrir db!", err);
            }else{
                console.log("Esta la db abierta: ", db.isOpen()? "si":"no");
                db.execSQL("CREATE TABLE IF NOT EXISTS favs (id INTEGER PRIMARY KEY AUTOINCREMENT, noticia TEXT)")
                .then(id=>{
                    fnOK(db);
                },error=>{
                    console.log("CREATE TABLE ERROR",error);
                    fnError(error);
                }
                );
               }
        });
    }

    addFavs(fav:string){
         this.getDb((db)=>{
            db.each("select count(*) from favs where noticia= (?)",[fav],
            (err,fila)=> {
                console.log(fila);
                if(fila == 0){
                    db.execSQL("insert into favs (noticia) values (?)",[fav],
                    (err,id)=> {
                        console.log("nuevo id: ", id);
                        Toast.show({text: `${fav} agregado`, duration:Toast.DURATION.SHORT});
                });        
                }else{
                    Toast.show({text: `${fav} ya existe como favorito`, duration:Toast.DURATION.SHORT});
                }
            },
            (err,totales)=> {}
            )
         },()=>{
             console.error("error on getDB");
         });
    }
}