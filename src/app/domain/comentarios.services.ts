import { Injectable } from "@angular/core";

@Injectable()
export class ComentariosServices{
     private comentarios =[];

     buscar(){
        if(this.comentarios.length == 0){
            this.add(1);
        }else{
            let i = this.comentarios.length;
            this.add(i+1);
        }
         return this.comentarios;
     }

     add(index){
        let comentario = {
            id: index,
            comentario: 'comentario: ' + index,
            nombre: 'Juan Perez',
            puntaje : '4.5'
        }
        this.comentarios.push(comentario);
     }
}