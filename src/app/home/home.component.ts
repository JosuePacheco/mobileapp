import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {Image} from "tns-core-modules/ui/image";
import * as camera from "nativescript-camera";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(){
        console.log("camera");
        const isAvailable = camera.isAvailable();
        console.log(isAvailable);
    camera.requestPermissions().then(
        function success() {
            camera.takePicture()
            .then((imageAsset) => {
                console.log("Size: " + imageAsset.options.width + "x" + imageAsset.options.height);
                console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                console.log("Photo saved in Photos/Gallery for Android or in Camera Roll for iOS");
            
            }).catch((err) => {
                console.log("Error -> " + err.message);
            });
        }, 
        function failure() {
            // permission request rejected
            // ... tell the user ...
        }
    );
    
}
}
