import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import { NoticiasEffects } from "./domain/noticias-state.model";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { reducersNoticias, intializeNoticiasState } from "./domain/noticias-state.model";
import { FavoritosService } from "./domain/favoritos.services";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import {NativeScriptFormsModule} from 'nativescript-angular/forms'


import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasServices } from "./domain/noticias.services";
import { NoticiasState } from "./domain/noticias-state.model";

export interface AppState{
    noticias:NoticiasState;
}
const reducers:ActionReducerMap<AppState> ={
    noticias: reducersNoticias
}
const reducersInitialState = {
    noticias: intializeNoticiasState()
}

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NativeScriptFormsModule,
        NgRxStoreModule.forRoot(reducers,{initialState:reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])

    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers:[NoticiasServices,FavoritosService]
})
export class AppModule { }
