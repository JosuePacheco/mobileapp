import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { DetailComponent } from "./detail/detail.component";
import { ComentariosServices } from "../domain/comentarios.services";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { SearchFormComponent } from "./form/search-form.component";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { MinLenDirective } from "../validators/minlen.validator";
import { SettingBusyComponent } from "../Utils/settingsBusy.component";



@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule
    ],
    declarations: [
        DetailComponent,
        MinLenDirective,
        SearchComponent,
        SearchFormComponent,
        SettingBusyComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [ComentariosServices]
})
export class SearchModule { }
