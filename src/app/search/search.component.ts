import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FavoritosService } from "./../domain/favoritos.services";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasServices } from "../domain/noticias.services";
import { RouterExtensions } from "nativescript-angular/router";
import { View, Color } from "tns-core-modules/ui/page";
import { GestureEventData } from "tns-core-modules/ui/gestures";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";
import { AppState } from "../app.module";
import {Store} from '@ngrx/store';
import { NuevaNoticiaAction, Noticia } from "../domain/noticias-state.model";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"/*,
    providers: [NoticiasServices]*/
})
export class SearchComponent implements OnInit {

    resultados:Array<string>;
    @ViewChild("layout", { static: false }) layout: ElementRef;
    constructor(private noticias:NoticiasServices,private routerExtensions: RouterExtensions, private favoritosService:FavoritosService,
        private store:Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.store.select(state=>state.noticias.sugerida)
                .subscribe(data=>{
                    const f = data;
                    if(f!= null){
                        Toast.show({text: "Sugerimos leer: " +f.titulo,duration:Toast.DURATION.SHORT});
                    }
                });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTapList(x){
        this.routerExtensions.navigate(['search','detail', x], {
            transition: {
                name: "fade"
            }
        });

    }

    tapText(args){
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    search(s:string){
        console.dir("buscar ahora",s);
        this.noticias.buscar(s).then((r:any)=>{
            console.log("resultados buscar: " + JSON.stringify(r));
            this.resultados =r;
        },(e)=>{
            console.log("error buscar ahora " +e);
            Toast.show({text:"error en la busqueda",duration: Toast.DURATION.SHORT});
        });
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color('blue'),
            duration:300,
            delay:150
        }).then(()=>{
            layout.animate({
                backgroundColor: new Color('white'),
                duration:300,
                delay:150
                
            });
        });

    }

    onItemLongTap(args: GestureEventData,noticia){
    console.log(noticia);
        const grid = <GridLayout> args.object;
        grid.animate({
            backgroundColor:new Color('blue'),
            duration:200
        }).then(()=>{
            grid.animate({
                backgroundColor : new Color('white'),
                duration:200
            });
        });

        dialogs.action("Mensaje","Cancelar!",["Eliminar","Archivar"])
                    .then((result)=>{
                        if(result === "Eliminar"){
                            Toast.show({text:"Has seleccionado eliminar",duration: Toast.DURATION.SHORT});
          
                        }else if(result ==="Archivar"){
                            Toast.show({text:"Has seleccionado archivar",duration: Toast.DURATION.SHORT});
                        }})
    }

    agregarFavoritos(x){
        this.favoritosService.addFavs(x);
    }

    verFavoritos(){
        this.routerExtensions.navigate(['featured'], {
            transition: {
                name: "fade"
            }
        });
    }
}


