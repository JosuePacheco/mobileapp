import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasServices } from "~/app/domain/noticias.services";
import { ActivatedRoute } from "@angular/router";
import { ComentariosServices } from "~/app/domain/comentarios.services";
import { EventData } from "tns-core-modules/ui/page";
import { ListViewEventData } from "nativescript-ui-listview";


@Component({
    selector: "Search",
    templateUrl: "./detail.component.html"/*,
    providers: [NoticiasServices]*/
})
export class DetailComponent implements OnInit {

    private index:number;
    private noticia='';
    private comentariosList =[];
    
    constructor(private noticias:NoticiasServices,private route:ActivatedRoute,public comentarioService:ComentariosServices) {
        // Use the component constructor to inject providers.
    }
 
    ngOnInit(): void {
        // Init your component properties here.
        this.route.params.subscribe(params=>{
            this.index = +params['id'];
            this.noticias.buscar("").then(s=>{
                this.noticia = s[this.index];
            });
        });

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onLoaded(event: EventData) {
        this.comentariosList = this.comentarioService.buscar();
    }

    onPullToRefreshInitiated(args: ListViewEventData){
        console.log('on pull refreshing');
        setTimeout( ()=> {
            this.comentariosList = this.comentarioService.buscar();
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }
    

}
