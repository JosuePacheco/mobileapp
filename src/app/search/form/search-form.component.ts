import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {MinLenDirective} from '../../validators/minlen.validator';

@Component({
    selector: 'SearchForm',
    template: `
                <FlexboxLayout flexDirection="row">
                    <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..."
                    required minlen="3"></TextField>
                    <Label *ngIf="texto.hasError('required')" text="*"></Label>
                    <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4"> </Label>
                </FlexboxLayout>
                <Button text="Buscar!" (tap)="onButtonTap()" *ngIf="texto.valid"></Button> 
 `
})
export class SearchFormComponent implements OnInit {
    
    textFieldValue:string='';
    @Output() search:EventEmitter<string> = new EventEmitter();
    @Input() inicial :string;
    constructor() { }

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
     }

    onButtonTap():void{
        console.log(this.textFieldValue);
        if(this.textFieldValue.length>2){
            this.search.emit(this.textFieldValue);
        }
    }
}
