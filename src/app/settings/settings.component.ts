import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    private edit:false;
    private nombre='';
    constructor() {
        // Use the component constructor to inject providers.
    }
    doLater(fn){
        setTimeout(fn,1000);
    }

    ngOnInit(): void {
        // Init your component properties here.
            this.nombre = appSettings.getString("nombre","");
    /*
        this.doLater(()=>{
            dialogs.action("Mensaje","Cancelar!",["Opcion1","Opcion2"])
                    .then((result)=>{
                        console.log('resultado',result);
                        if(result === "Opcion1"){
                            this.doLater(()=>{
                                dialogs.alert({
                                    title: 'titulo 1',
                                    message: 'mensaje 1',
                                    okButtonText: "btn1"
                                }).then(()=>{console.log('cerrado 1')});
                            });
                        }else if(result ==="Opcion2"){
                            this.doLater(()=>{
                                dialogs.alert({
                                    title: 'titulo 2',
                                    message: 'mensaje 2',
                                    okButtonText: "btn2"
                                }).then(()=>{console.log('cerrado 2');});
                            
                    })}
        })
    })
    */
   const toastOptions : Toast.ToastOptions = {text: "Hello World", duration:Toast.DURATION.SHORT};
   this.doLater(()=>{Toast.show(toastOptions)});
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    save(){
        appSettings.setString("nombre",this.nombre);
        this.edit=false;
    }
}
