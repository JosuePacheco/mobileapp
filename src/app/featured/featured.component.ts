import { Component, OnInit } from "@angular/core";
import { FavoritosService } from "./../domain/favoritos.services";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {
    favoritos=[];
    constructor(private favoritosService:FavoritosService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        console.log("inicializar");
        this.favoritosService.getDb((db)=>{
            db.all('select * from favs',
                (err,data)=> {
                    this.favoritos = data;
                    console.log(data);
                })
         },(error)=>{
             console.error("error on getDB");
         });

}

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    
   
}
