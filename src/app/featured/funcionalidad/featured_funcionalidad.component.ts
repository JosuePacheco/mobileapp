import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout";

@Component({
    selector: "Featured",
    templateUrl: "./featured_funcionalidad.component.html"
})
export class FeaturedFuncionalidadComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(args){
        const grid = <GridLayout> args.object;
        grid.rotate =0;
        grid.animate({
            rotate:360,
            duration:2000
        });
    }
}
