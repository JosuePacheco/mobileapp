import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { FeaturedFuncionalidadRoutingModule } from "./featured_funcionalidad-routing.module";
import { FeaturedFuncionalidadComponent } from "./featured_funcionalidad.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        FeaturedFuncionalidadRoutingModule
    ],
    declarations: [
        FeaturedFuncionalidadComponent,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class FeatureFuncionalidaddModule { }