import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { FeaturedComponent } from "./featured.component";

const routes: Routes = [
    { path: "", component: FeaturedComponent },
    { path: "funcionalidad", loadChildren: () => import("~/app/featured/funcionalidad/featured_funcionalidad.module").then((m) => m.FeatureFuncionalidaddModule) }

];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FeaturedRoutingModule { }
