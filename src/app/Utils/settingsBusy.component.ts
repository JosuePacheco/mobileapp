import { Component, OnInit } from '@angular/core';
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator"; 
@Component({
    selector: 'SettingBusy',
    template: `
    <Button text="Tocar!" (tap)="(activityIndicator.busy = !activityIndicator.busy)" class="btn btn-primary btn-active"></Button>
        <ActivityIndicator #activityIndicator busy="false" (busyChange)="cambio($event)" width="100" height="100px" class="activity-indicator">
        </ActivityIndicator> 
        
    `,
})
export class SettingBusyComponent implements OnInit {
    constructor() { }

    ngOnInit(): void { }

    cambio (e) {
        let indicator = <ActivityIndicator>e.object;
        console.log("indicator.busy: " + indicator.busy);
        } 
       
}


