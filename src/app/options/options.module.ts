import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { OptionRoutingModule } from "./options-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { OptionComponent } from "./options.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        OptionRoutingModule
    ],
    declarations: [
        OptionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class OptionModule { }